<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 07.11.12
 * Time: 1:48
 * To change this template use File | Settings | File Templates.
 */
namespace Infrastructure;

class ControllerFactory
{
    /**
     * @param $ControllerClass
     * @return \Controller\Abstraction\Controller
     */
    public function getController($ControllerClass)
    {
        return new $ControllerClass();
    }
}
