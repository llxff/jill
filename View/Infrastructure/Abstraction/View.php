<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 21:03
 * To change this template use File | Settings | File Templates.
 */
namespace View\Infrastructure\Abstraction;
use Application;
use View\Infrastructure\Utils\Html;

abstract class View
{
    private $ViewsPaths;

    private $layout;

    private $baseContent;

    public $Data = [];

    function __construct()
    {
        $this->ViewsPaths = $this->registerViewsPath();
    }

    public function Layout($layout)
    {
        $this->layout = $layout;
    }

    public function RenderController($Controller, $Action, $model)
    {
        return $this->RenderView($this->findTemplate($Controller, $Action, $this->ViewsPaths), $model);
    }

    public function RenderView($mainView, $model)
    {
        $toRender = $this->toRenderDataWith($model);

        $this->baseContent = $this->renderTemplate($mainView, $toRender);

        $renderedResult = $this->baseContent;

        if($this->layout !== null)
        {
            $renderedResult = $this->renderTemplate($this->layout, $toRender);
        }

        return $renderedResult;
    }

    public function toRenderDataWith($Model)
    {
        return ['view' => $this, 'Model' => $Model, 'Html' => new Html()];
    }

    public function BaseContent()
    {
        return $this->baseContent;
    }


    abstract protected function findTemplate($Controller, $Action, $Views);

    abstract protected function registerViewsPath();

    abstract protected function renderTemplate($view, $data);

}
