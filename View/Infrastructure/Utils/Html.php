<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 07.11.12
 * Time: 11:10
 * To change this template use File | Settings | File Templates.
 */
namespace View\Infrastructure\Utils;
use Infrastructure\SimpleRoute;
use Application;

class Html
{
    public function RenderAction($Controller, $Action)
    {
        $Route = new SimpleRoute();

        $Route->Controller = $Controller;
        $Route->Action = $Action;

        return Application::instance()->run($Route);
    }

    public function RenderTemplate($Template, $model = '')
    {
        return Application::instance()->getView()->RenderView($Template, $model);
    }
}
