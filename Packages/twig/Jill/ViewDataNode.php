<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 23:11
 * To change this template use File | Settings | File Templates.
 */
namespace Packages\twig\Jill;

class ViewDataNode extends \Twig_Node
{
    public function __construct($name, \Twig_Node_Expression $value, $lineno, $tag = null)
    {
        parent::__construct(array('value' => $value), array('name' => $name), $lineno, $tag);
    }

    public function compile(\Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write('$context["view"]->Data["'.$this->getAttribute('name').'"]=')
            ->subcompile($this->getNode('value'))
            ->raw(";\n")
        ;
    }
}
