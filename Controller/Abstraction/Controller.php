<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */
namespace Controller\Abstraction;
use View\Infrastructure\Abstraction\View;

abstract class Controller
{
    /**
     * @var View
     */
    protected $View;

    private $action;

    public function setView(View $View)
    {
        $this->View = $View;
    }

    protected function View($Model = '')
    {
        return $this->ViewOf($this->action, $Model);
    }

    protected function getControllerNameForView()
    {
        $ClassParts = explode('\\', get_class($this));

        return str_replace('Controller', '', $ClassParts[sizeof($ClassParts) - 1]);
    }

    protected function ViewOf($Action, $Model = '')
    {
        return $this->View->RenderController($this->getControllerNameForView(), $Action, $Model);
    }

    public function run($action, array $args = array())
    {
        $this->action = $action;

        $reflection = new \ReflectionMethod($this, $action);

        $pass = array();

        foreach($reflection->getParameters() as $param)
        {
            if(isset($args[$param->getName()]))
            {
                $pass[] = $args[$param->getName()];
            }
            else if($param->isOptional())
            {
                $pass[] = $param->getDefaultValue();
            }
            else
            {
                $pass[] = null;
            }
        }

        return $reflection->invokeArgs($this, $pass);
    }

    /**
     * @param \Exception $e
     * @return null
     */
    public function onException(\Exception $e)
    {
        throw $e;
    }
}
