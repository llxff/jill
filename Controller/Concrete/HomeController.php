<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 21:17
 * To change this template use File | Settings | File Templates.
 */
namespace Controller\Concrete;
use Controller\Abstraction\Controller;

class HomeController extends Controller
{
    public function Index()
    {
        return $this->View(['hello' => 'Hi, Jill!']);
    }
}
