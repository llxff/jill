<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 21:43
 * To change this template use File | Settings | File Templates.
 */
use Infrastructure\SimpleRoute;
use Infrastructure\BadCode\BadCodeException;
use Infrastructure\ControllerFinder;
use Infrastructure\ControllerFactory;
use View\Infrastructure\Concrete\DefaultView;

class Application
{
    /**
     * @var Application
     */
    private static $app;

    public static function instance()
    {
        if(self::$app === null)
        {
            return self::$app = new self();
        }

        self::$app->RegisterVariousServices();
        return self::$app;
    }

    function __construct()
    {
        set_error_handler(function($c, $m, $f, $l) { throw new BadCodeException($m, $c, $f, $l); }, E_ALL);

        $this->RegisterPermanentServices();
        $this->RegisterVariousServices();
    }

    /**
     * @var \View\Infrastructure\Abstraction\View
     */
    private $View;

    protected function setView($View)
    {
        $this->View = $View;
    }

    public function getView()
    {
        return $this->View;
    }

    /**
     * @var \Infrastructure\ControllerFinder
     */
    private $ControllerFinder;

    protected function setControllerFinder($ControllerFinder)
    {
        $this->ControllerFinder = $ControllerFinder;
    }

    protected function getControllerFinder()
    {
        return $this->ControllerFinder;
    }

    /**
     * @var \Infrastructure\ControllerFactory
     */
    private $ControllerFactory;

    protected function setControllerFactory($ControllerFactory)
    {
        $this->ControllerFactory = $ControllerFactory;
    }

    protected function getControllerFactory()
    {
        return $this->ControllerFactory;
    }


    public function run(SimpleRoute $Route)
    {

        $Route->define();

        $ControllerFinder = $this->getControllerFinder();
        $ControllerFinder->find($Route->Controller);

        if($ControllerFinder->isControllerFounded())
        {
            $ControllerClass = $ControllerFinder->getController();

            $Controller = $this->getControllerFactory()->getController($ControllerClass);
            $Controller->setView($this->getView());

            try
            {
                return $Controller->run($Route->Action, $_REQUEST);
            }
            catch(\Exception $e)
            {
                return $Controller->onException($e);
            }
        }

        header('Status: 404 Not Found');
        return '';
    }

    public function RegisterPermanentServices()
    {
        $this->setControllerFinder(new ControllerFinder());
        $this->setControllerFactory(new ControllerFactory());
    }

    public function RegisterVariousServices()
    {
        $this->setView(new DefaultView());
    }
}
